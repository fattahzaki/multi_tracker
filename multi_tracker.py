import argparse
import json
import logging
import multiprocessing
import os
import shutil
import time

import cv2
from imutils.video import FPS
from skimage.metrics import structural_similarity as ssim

from detector import Detector
from pump_bay import PumpBay
from trackable_object import TrackableObject
from utils import get_centroid

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

#VIDEO = "temasya-short2_720p_264.mp4"
#VIDEO = "temasya-short2_720p_264_corner_case.mp4"
VIDEO = "../kk_collaterals/501_4_good.mp4"
CAP = cv2.VideoCapture(VIDEO)
NUM_FRAMES_SKIP = 30
SSMI_THRESH = 0.7

def is_valid_json(parser, arg):
    """Sanity check for JSON file provided but the user"""

    if not os.path.exists(arg):
        parser.error(f"The file {arg} does not exist")
        return arg
    if not arg.endswith(".json"):
        parser.error(f"The file {arg} is not a JSON file")
        return arg

    # TODO: json schema check

    return arg

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config_file", required=True,
                        type=lambda x: is_valid_json(parser, x))
    return parser.parse_args()

def init_pumps(data):
    """Initialize PumpBay object with relevant information

    Initialize each pump bay with relevant information

    Parameters
    ----------
    data : dict
        Relevant pump data extracted from user-defined JSON file

    Returns
    -------
        list
            List of pump objects that have been initialized
    """

    pumps = []
    pumps_data = data["pump_bays"]
    logs_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                            data["logs_dir"])
    if os.path.exists(logs_dir):
        shutil.rmtree(logs_dir)
    os.makedirs(logs_dir)

    for pump_data in pumps_data:
        pump = PumpBay(pump_data, data["station_id"])
        pumps.append(pump)
    return pumps

# TODO: Use af's VideoCap object
def read_frames(q):
    cap = cv2.VideoCapture(VIDEO)
    while True:
        ret, frame = cap.read()
        if not ret:
            break
        pos = cap.get(cv2.CAP_PROP_POS_FRAMES)
        if not q.full():
            q.put((pos, frame))

def run(pump_bays):
    trackers = {}
    detection_id = 0

    detector = Detector("YOLO", "yolov3-320/yolov3.weights", "yolov3-320/yolov3.cfg", "yolov3-320/coco.names")

    frames_q = multiprocessing.Queue()
    read_t = multiprocessing.Process(target=read_frames, args=(frames_q,),
                                     daemon=True)
    read_t.start()
    logger.info(f"Preparing video capture {VIDEO}. Please wait...")
    time.sleep(2)
    logger.info(f"Video capture {VIDEO} is ready")

    fps = FPS().start()

    while True:
        # TODO: break condition
        if frames_q.empty():
            continue
        frame_pos, frame = frames_q.get()

        # TODO: multiprocessing.Pool
        for pump in pump_bays:
            # Draw pump ROI
            cv2.rectangle(frame, (pump.roi[0], pump.roi[1]), (pump.roi[0]+pump.roi[2], pump.roi[1]+pump.roi[3]), (255, 255, 255), 2)

            # Run detection
            if frame_pos % NUM_FRAMES_SKIP == 0:
                boxes = detector.detect(frame, ["car", "bus", "truck"], roi=pump.roi)

                if len(boxes) == 0:
                    if pump.reidentify:
                        logger.info(f"Vehicle {pump.vehicle.id} has exited pump {pump.id}")
                        del trackers[pump.vehicle.id]
                        pump.remove_vehicle()
                        pump.reidentify = False
                elif len(boxes) == 1:
                    box = boxes[0]
                    rect = (box[2][0], box[2][1], box[2][2], box[2][3])
                    centroid = get_centroid(rect[0], rect[1], rect[2], rect[3])
                    img = frame[pump.roi[1]:pump.roi[1]+pump.roi[3], pump.roi[0]:pump.roi[0]+pump.roi[2]]
                    if pump.vehicle == None:
                        logger.info(f"Vehicle detected in pump {pump.id}. ID number {detection_id} assigned")
                        vehicle = TrackableObject(detection_id, centroid, img)
                        pump.update_vehicle(vehicle)
                        detection_id += 1
                    else:
                        vehicle = pump.vehicle

                    if pump.reidentify:
                        curr_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                        idle_img  = cv2.cvtColor(vehicle.img, cv2.COLOR_BGR2GRAY)
                        score, diff = ssim(curr_img, idle_img, full=True)
                        if score > SSMI_THRESH:
                            logger.info(f"Vehicle {pump.vehicle.id} reidentified in pump {pump.id}")
                        else:
                            logger.info(f"Vehicle {pump.vehicle.id} has exited pump {pump.id}")
                            del trackers[vehicle.id]
                            pump.remove_vehicle()
                            continue
                        pump.reidentify = False

                    tracker = cv2.legacy.TrackerMOSSE_create()
                    tracker.init(frame, rect)
                    trackers[vehicle.id] = tracker

            else:
                vehicle = pump.vehicle
                if vehicle == None:
                    continue
                tracker = trackers[vehicle.id]
                success, box = tracker.update(frame)
                if success:
                    rect = [int(v) for v in box]
                    if not pump.vehicle.update(rect, pump.roi, frame):
                        pump.reidentify = True

        # TODO: draw fps counter
        cv2.imshow("Frame", frame)
        if cv2.waitKey(1) & 0xFF == ord("q"):
            break

        fps.update()

    fps.stop()
    logger.info("Elapsed time: {:.2f}".format(fps.elapsed()))
    logger.info("FPS: {:.2f}".format(fps.fps()))

    cv2.destroyAllWindows()
    CAP.release()

if __name__ == "__main__":
    args = parse_args()
    with open(args.config_file, "r") as f:
        config_dict = json.load(f)

    pumps = init_pumps(config_dict)
    if len(pumps) == 0:
        logger.error("No pumps data found")
        sys.exit(1)

    run(pumps)

