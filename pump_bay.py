
class PumpBay:
    def __init__(self, data, station_id):
        self.id = data["pump_id"]
        self.station_id = station_id
        self.roi = (data["roi"]["x"], data["roi"]["y"], data["roi"]["w"], data["roi"]["h"])
        self.vehicle = None
        self.reidentify = False

    def update_vehicle(self, vehicle):
        self.vehicle = vehicle

    def remove_vehicle(self):
        self.vehicle = None
