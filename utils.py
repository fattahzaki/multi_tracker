
def get_centroid(x, y, w, h):
    return (int(x + w/2.0), int(y + h/2.0))

def is_point_out_of_box(point, box):
    if point[0] < box[0] or \
       point[0] > box[0] + box[2] or \
       point[1] < box[1] or \
       point[1] > box[1] + box[3]:
           return True
    return False

