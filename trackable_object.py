
from collections import deque
from copy import deepcopy

import cv2
from scipy.spatial import distance as dist

from utils import get_centroid, is_point_out_of_box

class TrackableObject:
    def __init__(self, object_id, centroid, img):
        self.id = object_id
        self.centroids = [None, None]
        self.add_centroid(centroid)
        self.img = deepcopy(img)
        self.max_distance = 0.5
        self.idle_count = 0
        self.max_idle = 10
        self.moving_count = 0
        self.max_moving = 10
        self.disappeared_count = 0
        self.max_disappeared = 3
        self.status = "NEW"

    def add_centroid(self, centroid):
        self.centroids[1] = self.centroids[0]
        self.centroids[0] = centroid

    def update(self, box, pump_roi, frame):
        (x, y, w, h) = box
        centroid = get_centroid(x, y, w, h)
        if is_point_out_of_box(centroid, pump_roi):
            self.disappeared_count += 1
            if self.disappeared_count > self.max_disappeared:
                return False

        self.add_centroid(centroid)
        prev_centroid = self.centroids[1]
        curr_centroid = self.centroids[0]
        if prev_centroid != None and curr_centroid != None:
            distance = dist.euclidean(curr_centroid, prev_centroid)
            #print(f"Vehicle {self.id}: Distance: {distance}")
            if distance > self.max_distance:
                self.moving_count += 1
            else:
                self.idle_count += 1

        if self.idle_count > self.max_idle:
            if self.idle_count - 1 == self.max_idle:
                self.img = frame[pump_roi[1]:pump_roi[1]+pump_roi[3], pump_roi[0]:pump_roi[0]+pump_roi[2]]
                cv2.imwrite(f"{self.id}_idle.png", self.img)
            self.status = "IDLE"
            self.moving_count = 0
            self.idle_count = 0
        elif self.moving_count > self.max_moving:
            self.status = "MOVING"
            self.idle_count = 0
            self.moving_count = 0

        #print(f"Vehicle {self.id}: Status: {self.status}")
        #print(f"Vehicle {self.id}: Moving count: {self.moving_count}")
        #print(f"Vehicle {self.id}: Idle count: {self.idle_count}")
        if self.status == "MOVING":
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            text = f"Vehicle {self.id} moving"
            cv2.putText(frame, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX,
                        1, (0, 255, 0), 2)
        elif self.status == "IDLE":
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 165, 255), 2)
            text = f"Vehicle {self.id} stopping"
            cv2.putText(frame, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX,
                        1, (0, 165, 255), 2)

        return True

