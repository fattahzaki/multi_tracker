import time
from copy import deepcopy

import cv2
import numpy as np

CONFIDENCE_THRESH = 0.8
NMS_THRESH = 0.3

class Detector:
    def __init__(self, net_type, weights, config, labels):
        self.net_type = net_type
        self.net = cv2.dnn.readNetFromDarknet(config, weights)
        layer_names = self.net.getLayerNames()
        self.output_layers = [layer_names[i[0] - 1] for i in \
                              self.net.getUnconnectedOutLayers()]
        self.all_labels = open(labels).read().strip().split("\n")

    def detect(self, img, labels=None, roi=None):
        if self.net_type == "YOLO":
            return self._yolo_detect(img, labels=labels, roi=roi)

    def _yolo_detect(self, img, labels=None, roi=None):
        if roi != None:
            frame = deepcopy(img[roi[1]:roi[1]+roi[3], roi[0]:roi[0]+roi[2]])
        else:
            frame = deepcopy(img)
        (H, W) = frame.shape[:2]
        blob = cv2.dnn.blobFromImage(frame, 0.00392, (416, 416), (0, 0, 0), True,
                                     crop=False)
        self.net.setInput(blob)
        start = time.time()
        outputs = self.net.forward(self.output_layers)
        end = time.time()
        #print("Yolo took {:.6f} seconds".format(end - start))

        res_boxes = []
        boxes = []
        confidences = []
        class_ids = []

        for output in outputs:
            for detection in output:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]

                if confidence > CONFIDENCE_THRESH:
                    box = detection[0:4] * np.array([W, H, W, H])
                    (c_x, x_y, width, height) = box.astype("int")

                    x = int(c_x - (width / 2))
                    y = int(x_y - (height / 2))

                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    class_ids.append(class_id)

        indexes = cv2.dnn.NMSBoxes(boxes, confidences, CONFIDENCE_THRESH, \
                                   NMS_THRESH)
        if len(indexes) > 0:
            for i in indexes.flatten():
                if labels != None and not self.all_labels[class_ids[i]] in labels:
                    continue
                (x, y, w, h) = (boxes[i][0], boxes[i][1], boxes[i][2], boxes[i][3])
                if roi != None:
                    x = roi[0] + x
                    y = roi[1] + y
                res_boxes.append((class_ids[i], confidences[i], (x, y, w, h)))
        return res_boxes
